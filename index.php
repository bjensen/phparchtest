<?php
require 'vendor/autoload.php';

error_reporting(E_ALL);
ini_set('display_startup_errors', 1);
ini_set('display_errors', 1);

echo '<h1>Hello World!</h1>';
echo '<p>'. gethostname() . '</p>';

// Test MySQL
$options = [
    PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
    PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
    PDO::ATTR_EMULATE_PREPARES   => false,
];

try {
    $pdo = new PDO('mysql:host=vm-upd-pds;dbname=testdb', 'test', 'test', $options);

    $stmt = $pdo->prepare('SELECT * FROM testdb.test');
    $stmt->execute();
    $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);

    echo '<ul>';
    foreach ($rows as $row) {
        echo '<li>' . $row['name'] . '</li>';
    }
    echo '</ul>';
} catch(Exception $e) {
    echo $e->getMessage();
}

// Test Redis
try {
    $client = new Predis\Client('tcp://vm-upd-pds:6379');
    $viewNum = $client->incr('views');

    echo '<p>' . $viewNum . ' views</p>';
}
catch (\Exception $e) {
    echo $e->getMessage();
}

// Test NFS (session sharing)
session_start([
    'save_path' => '/opt/data/sessions'
]);

if (! array_key_exists('uuid', $_SESSION)) {
    $_SESSION['uuid'] = uniqid();
}

echo '<p>UUID: ' . $_SESSION['uuid'] . '</p>';
